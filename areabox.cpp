#include<iostream>
#include "boxArea.h"
#include "boxVolume.h"
#include <cmath>
#ifdef boxArea
#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif
#endif
using namespace std;
int main()
{
    float length,width,height;
    cout<<"Enter The Length Width And Height Of The Box:"<<endl;
    cin>>length>>width>>height;
    if(!isdigit(length)&&!isdigit(width)&&!isdigit(height))
    {
        cout<<"Enter Values As Numbers"<<endl;
    }
    else{
    boxArea(length,width,height);
    boxVolume(length,width,height);
}
}
